﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BracketSystem.Core.Models.Entities;

namespace BracketSystem.Core.Data
{
    #region Interfaces

    public interface ITeamRepo : IGenericRepository<Team>
    {
        #region Methods

        Task<List<Team>> FindTeams(int page = 1, int pageSize = 10,
            string filter = null, string sortColumn = "Name", string sortOrder = "asc");

        Task<List<Team>> GetAllRecordsFromDatabase();

        Task<int> CountTeamsAsync(string filter);

        #endregion
    }
}

#endregion