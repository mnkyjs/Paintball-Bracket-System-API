﻿using System;
using System.Threading.Tasks;

namespace BracketSystem.Core.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IMatchScheduleRepo Matches { get; }
        ITeamRepo Teams { get; }
        IFeatureRepo Features { get; }
        Task CompleteAsync();
    }
}
