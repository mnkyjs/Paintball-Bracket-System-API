﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BracketSystem.Core.Models.Dtos;
using BracketSystem.Core.Models.Entities;

namespace BracketSystem.Core.Data
{
    public interface IMatchScheduleRepo : IGenericRepository<Match>
    {
        Task<List<TeamDto[]>> CreateSchedule(List<TeamDto> teams, DateTime date, string userIdentifier,
            string location, string name);

        Task<List<Match>> DeleteMatches(string userIdentifier);

        Task<BlockForViewDto> GetMatchesByGuid(string guid);

        Task<List<MatchForViewDto>> GetAllMatches();

        Task<List<Match>> DeleteMatch(string userIdentifier, string guid);
    }
}
