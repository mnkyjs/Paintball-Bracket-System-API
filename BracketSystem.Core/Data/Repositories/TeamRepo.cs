﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BracketSystem.Core.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace BracketSystem.Core.Data.Repositories
{
    public class TeamRepo : GenericRepository<Team>, ITeamRepo
    {
        #region Constructors

        public TeamRepo(BracketContext context)
            : base(context)
        {
        }

        #endregion Constructors

        #region BracketContext

        public BracketContext BracketContext => DbContext as BracketContext;

        #endregion BracketContext

        #region Methods

        public async Task<List<Team>> FindTeams(int page, int pageSize, string filter,
            string sortColumn,
            string sortOrder)
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Name" : sortColumn;
            var sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<Team> query;

            if (string.IsNullOrEmpty(filter))
            {
                query = BracketContext.Teams.Where(team => team.Name.ToLower() != "wildcard");
            }
            else
            {
                filter = filter.ToLower();
                query = BracketContext.Teams
                    .Where(p => p.Name.ToLower().Contains(filter) && p.Name.ToLower() != "wildcard");
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Apply paging
            var items = query.Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return await items;
        }

        /// <summary>
        ///     Returns the number of teams that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of events matching the given filter string</returns>
        public async Task<int> CountTeamsAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter)) return await BracketContext.Teams.CountAsync();

            filter = filter.ToLower();

            return await BracketContext.Teams
                .Where(e => e.Name.ToLower().Contains(filter))
                .CountAsync();
        }

        public async Task<List<Team>> GetAllRecordsFromDatabase()
        {
            var tempTeams = await BracketContext.Teams.ToListAsync();

            return tempTeams.Where(team => team.Name != "pause").ToList();
        }

        #endregion Methods
    }
}