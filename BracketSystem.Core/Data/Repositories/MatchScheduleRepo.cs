﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BracketSystem.Core.Models.Dtos;
using BracketSystem.Core.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace BracketSystem.Core.Data.Repositories
{
    public class MatchScheduleRepo : GenericRepository<Match>, IMatchScheduleRepo
    {
        public MatchScheduleRepo(DbContext context)
            : base(context)
        {
        }

        #region BracketContext

        public BracketContext BracketContext => DbContext as BracketContext;

        #endregion BracketContext

        public async Task<List<TeamDto[]>> CreateSchedule(List<TeamDto> teams, DateTime date, string userIdentifier,
            string location, string name)
        {
            var matches = new List<TeamDto[]>();

            var tempTeam = BracketContext.Teams.FirstOrDefaultAsync(n => n.Name == "wildcard");

            if (teams.Count % 2 == 1)
                teams.Add(new TeamDto
                {
                    Id = tempTeam.Result.Id,
                    Name = tempTeam.Result.Name
                });

            for (var i = 0; i < teams.Count - 1; i++)
            {
                var mid = teams.Count / 2;
                var teamListOne = teams.GetRange(0, mid);
                var teamListTwo = teams.GetRange(mid, teams.Count - mid);


                teamListOne.Reverse();

                if (i % 2 == 0)
                    for (var f = 0; f <= teamListOne.Count - 1; f++)
                    {
                        var match = new TeamDto[2];
                        match[0] = teamListOne[f];
                        match[1] = teamListTwo[f];

                        matches.Add(match);
                    }
                else
                    for (var f = 0; f <= teamListOne.Count - 1; f++)
                    {
                        var match = new TeamDto[2];
                        match[1] = teamListOne[f];
                        match[0] = teamListTwo[f];

                        matches.Add(match);
                    }

                var tempCounterToRemove = teams.Count - 1;
                var teamToAddAtIndexOne = teams.ElementAt(tempCounterToRemove);
                teams.RemoveAt(tempCounterToRemove);
                teams.Insert(1, teamToAddAtIndexOne);
            }

            await SaveMatches(matches, userIdentifier, date, location, name);
            return matches;
        }

        public async Task<List<Match>> DeleteMatches(string userIdentifier)
        {
            var allMatches = await BracketContext.Matches.Where(u => u.UserIdentifier == userIdentifier).ToListAsync();
            return allMatches;
        }

        public async Task<BlockForViewDto> GetMatchesByGuid(string guid)
        {
            var matches = BracketContext.Matches.Include(a => a.TeamA).Include(b => b.TeamB)
                .Where(x => x.Guid == guid)
                .ToList();
            var listOfMatchesWithDate = await ShowMatchesForView(matches);
            var response = new BlockForViewDto
            {
                Name = matches.FirstOrDefault()?.MatchName,
                Blocks = listOfMatchesWithDate,
                UserIdentifier = matches.FirstOrDefault()?.UserIdentifier,
                Location = matches.FirstOrDefault()?.Location
            };
            return response;
        }

        public async Task<List<Match>> DeleteMatch(string userIdentifier,
            string guid)
        {
            var matches = BracketContext.Matches.Where(match => match.Guid == guid)
                .Where(u => u.UserIdentifier == userIdentifier)
                .ToListAsync();
            return await matches;
        }

        public async Task<List<MatchForViewDto>> GetAllMatches()
        {
            var matches = await BracketContext.Matches.Select(match => new MatchForViewDto
            {
                Name = match.MatchName,
                Guid = match.Guid,
                Date = match.Date,
                Location = match.Location,
                UserIdentifier = match.UserIdentifier
            }).ToListAsync();

            var result = matches.GroupBy(dto => dto.Guid).Select(dtos =>
            {
                var result = dtos.First();
                result.BlockCount = (dtos.Count() + 1) / 2;
                return result;
            }).Where(dto => dto.Date >= DateTime.Today).OrderBy(dto => dto.Date).ToList();

            return result;
        }

        private async Task<IEnumerable<BlockDto>> ShowMatchesForView(List<Match> matches)
        {
            var result = new List<BlockDto>();
            var counter = 0;
            await Task.Run(() =>
            {
                for (var i = 0; i < matches.Count; i++)
                {
                    if (matches.Count % 2 == 1 || i == matches.Count - 1)
                        if (i == matches.Count - 1)
                        {
                            var index = matches.Count - 1;
                            result.Add(new BlockDto
                            {
                                BlockNumber = counter + 1,
                                Games = new List<string>
                                {
                                    $"{matches[index].TeamA.Name} - {matches[index].TeamB.Name}"
                                }
                            });
                            break;
                        }

                    var block = new BlockDto
                    {
                        BlockNumber = counter + 1,
                        Games = new List<string>
                        {
                            $"{matches[i].TeamA.Name} - {matches[i].TeamB.Name}",
                            $"{matches[i + 1].TeamA.Name} - {matches[i + 1].TeamB.Name}"
                        }
                    };
                    result.Add(block);
                    counter++;
                    i++;
                }
            });

            return result;
        }

        private async Task SaveMatches(IEnumerable<TeamDto[]> matches, string userIdentifier,
            DateTime date,
            string location, string clashName)
        {
            var guid = Guid.NewGuid().ToString();
            var dbMatches = new List<Match>();

            foreach (var item in matches)
            {
                var dayMatch = new Match
                {
                    TeamAId = item[0].Id ?? default(int),
                    TeamBId = item[1].Id ?? default(int),
                    Date = date,
                    Guid = guid,
                    MatchName = clashName,
                    Location = location,
                    UserIdentifier = userIdentifier
                };
                BracketContext.Entry(dayMatch).State = EntityState.Added;
                dbMatches.Add(dayMatch);
            }

            await BracketContext.Matches.AddRangeAsync(dbMatches);
        }
    }
}
