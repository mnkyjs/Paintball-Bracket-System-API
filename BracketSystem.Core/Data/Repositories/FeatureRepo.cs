using BracketSystem.Core.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace BracketSystem.Core.Data.Repositories
{
    public class FeatureRepo: GenericRepository<Feature>, IFeatureRepo
    {
        public FeatureRepo(DbContext dbContext) : base(dbContext)
        {
        }
        
        #region BracketContext

        public BracketContext BracketContext => DbContext as BracketContext;

        #endregion BracketContext
    }
}
