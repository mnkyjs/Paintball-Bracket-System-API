﻿using System;
using System.Threading.Tasks;
using BracketSystem.Core.Models.Entities;
using Microsoft.Extensions.Logging;

namespace BracketSystem.Core.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BracketContext _context;
        private readonly ILogger<UnitOfWork> _logger;
        private FeatureRepo _featureRepo;
        private MatchScheduleRepo _matchScheduleRepo;
        private TeamRepo _teamRepo;

        public UnitOfWork(BracketContext context, ILogger<UnitOfWork> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IFeatureRepo Features => _featureRepo ??= new FeatureRepo(_context);

        public IMatchScheduleRepo Matches =>
            _matchScheduleRepo ??= new MatchScheduleRepo(_context);

        // Fields
        public ITeamRepo Teams => _teamRepo ??= new TeamRepo(_context);

        public async Task CompleteAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"Something went wrong: {ex}");
                Console.WriteLine(ex);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
