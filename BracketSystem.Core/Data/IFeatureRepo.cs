using BracketSystem.Core.Models.Entities;

namespace BracketSystem.Core.Data
{
    public interface IFeatureRepo : IGenericRepository<Feature>
    {
    }
}
