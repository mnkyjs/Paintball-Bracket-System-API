﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BracketSystem.Core.Migrations
{
    public partial class removefieldsentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Paintballfields_PaintballfieldId",
                table: "Matches");

            migrationBuilder.DropTable(
                name: "Paintballfields");

            migrationBuilder.DropIndex(
                name: "IX_Matches_PaintballfieldId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "PaintballfieldId",
                table: "Matches");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Matches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location",
                table: "Matches");

            migrationBuilder.AddColumn<int>(
                name: "PaintballfieldId",
                table: "Matches",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Paintballfields",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HouseNumber = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Place = table.Column<string>(type: "text", nullable: true),
                    PostalCode = table.Column<int>(type: "integer", nullable: false),
                    Street = table.Column<string>(type: "text", nullable: true),
                    UserIdentifier = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paintballfields", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Matches_PaintballfieldId",
                table: "Matches",
                column: "PaintballfieldId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_Paintballfields_PaintballfieldId",
                table: "Matches",
                column: "PaintballfieldId",
                principalTable: "Paintballfields",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
