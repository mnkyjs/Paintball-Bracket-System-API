using BracketSystem.Core.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

public class BracketContextFactory : IDesignTimeDbContextFactory<BracketContext>
{
    public BracketContext CreateDbContext(string[] args)
    {
        // Configure DB
        var optionsBuilder = new DbContextOptionsBuilder<BracketContext>();

        // Build an IConfigurationRoot using environment variables
        var configuration = new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build();

        // Retrieve connection string from $DB_CONNECTION_STRING 
        optionsBuilder.UseNpgsql("Server=localhost;Database=PaintballBracket;User Id=pb_user;Password=_p4sSw0rd!");

        return new BracketContext(optionsBuilder.Options);
    }
}