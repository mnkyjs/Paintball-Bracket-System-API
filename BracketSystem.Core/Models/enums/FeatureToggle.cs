using System.Text.Json.Serialization;

namespace BracketSystem.Core.Models.enums
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum FeatureToggle
    {
        ShowSidebarNavigation
    }
}
