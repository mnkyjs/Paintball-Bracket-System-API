﻿using System.Collections.Generic;
using BracketSystem.Core.Models.Entities;

namespace BracketSystem.Core.Models.Dtos
{
    public class CreateScheduleDto
    {
        public string Date { get; set; }
        public List<TeamDto> Teams { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
    }
}
