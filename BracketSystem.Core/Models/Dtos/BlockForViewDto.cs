using System.Collections.Generic;

namespace BracketSystem.Core.Models.Dtos
{
    public class BlockForViewDto
    {
        public string Name { get; set; }
        public string UserIdentifier { get; set; }
        public string Location { get; set; }
        public IEnumerable<BlockDto> Blocks { get; set; }
    }
}
