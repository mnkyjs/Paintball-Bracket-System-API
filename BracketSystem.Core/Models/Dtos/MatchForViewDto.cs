using System;

namespace BracketSystem.Core.Models.Dtos
{
    public class MatchForViewDto
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public string Location { get; set; }

        public int BlockCount { get; set; }

        public string UserIdentifier { get; set; }
    }
}
