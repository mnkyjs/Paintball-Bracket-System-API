﻿using System.ComponentModel.DataAnnotations;
using BracketSystem.Core.Models.Entities;

namespace BracketSystem.Core.Models.Dtos
{
    public class TeamDto
    {
        public TeamDto()
        {
        }

        public TeamDto(Team team) : this()
        {
            FromEntity(team);
        }

        public int? Id { get; set; }
        public string UserIdentifier { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "You must specify password between 2 and 50 characters")]
        public string Name { get; set; }

        public void FromEntity(Team entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            UserIdentifier = entity.UserIdentifier;
        }
    }
}
