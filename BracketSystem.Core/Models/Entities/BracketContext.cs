﻿using System;
using BracketSystem.Core.Models.enums;
using Microsoft.EntityFrameworkCore;

namespace BracketSystem.Core.Models.Entities
{
    public class BracketContext : DbContext
    {
        public BracketContext(DbContextOptions<BracketContext> options) : base(options)
        {
        }

        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Match> Matches { get; set; }
        public virtual DbSet<Feature> Features { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Feature>()
                .Property(feature => feature.FeatureToggle)
                .HasConversion(
                    v => v.ToString(),
                    v => (FeatureToggle)Enum.Parse(typeof(FeatureToggle), v));
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
