﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BracketSystem.Core.Models.Entities
{
    public class Match
    {
        public int Id { get; set; }
        public string MatchName { get; set; }
        public DateTime? Date { get; set; }

        public string Guid { get; set; }

        public string UserIdentifier { get; set; }

        public string Location { get; set; }

        [ForeignKey("TeamA")] public int TeamAId { get; set; }

        public virtual Team TeamA { get; set; }

        [ForeignKey("TeamB")] public int TeamBId { get; set; }

        public virtual Team TeamB { get; set; }
    }
}
