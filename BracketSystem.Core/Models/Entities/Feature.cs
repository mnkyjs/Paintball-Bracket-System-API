using BracketSystem.Core.Models.enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BracketSystem.Core.Models.Entities
{
    public class Feature
    {
        public int Id { get; set; }

        public FeatureToggle FeatureToggle { get; set; }

        public bool IsEnabled { get; set; }
    }
}
