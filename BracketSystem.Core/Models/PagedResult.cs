using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BracketSystem.Core.Models
{
    /// <summary>
    ///     Represents a paged result of an API query.
    /// </summary>
    /// <typeparam name="T">The type of objects contained in this result</typeparam>
    public class PagedResult<T>
    {
        /// <summary>
        ///     Constructs a new PagedResult object.
        /// </summary>
        public PagedResult()
        {
            Items = new List<T>();
            Metadata = new Metadata(0, 0, 0);
        }

        /// <summary>
        ///     Constructs a new PagedResult object.
        /// </summary>
        /// <param name="items">The items to be included in the result</param>
        /// <param name="count">The maximum number of items that the query returned</param>
        /// <param name="pageNumber">The current results page number</param>
        /// <param name="pageSize">The number maximum number of items contined in this result object</param>
        public PagedResult(List<T> items, int count, int pageNumber, int pageSize)
        {
            Metadata = new Metadata(count, pageNumber, pageSize);
            Items = items;
        }


        #region Properties

        /// <summary>
        ///     The items contained in this result.
        /// </summary>
        [JsonPropertyName("items")]
        public IEnumerable<T> Items { get; set; }

        /// <summary>
        ///     The metadata object describing this result.
        /// </summary>
        [JsonPropertyName("metadata")]
        public Metadata Metadata { get; set; }

        #endregion
    }

    /// <summary>
    ///     A metadata object used for describing the paged results of an API query.
    /// </summary>
    public class Metadata
    {
        /// <summary>
        ///     Constructs a new MataData object.
        /// </summary>
        /// <param name="count">The total number of results</param>
        /// <param name="pageNumber">The current page's number</param>
        /// <param name="pageSize">The maximum number of entries per page</param>
        public Metadata(int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            CurrentPage = pageNumber;
            PageSize = pageSize;
            TotalPages = (int) Math.Ceiling(count / (double) pageSize);
        }


        #region Properties

        /// <summary>
        ///     The current page's number.
        /// </summary>
        [JsonPropertyName("currentPage")]
        public int CurrentPage { get; set; }

        /// <summary>
        ///     The total number of available pages.
        /// </summary>
        [JsonPropertyName("totalPages")]
        public int TotalPages { get; set; }

        /// <summary>
        ///     The page's maximum number of results.
        /// </summary>
        [JsonPropertyName("pageSize")]
        public int PageSize { get; set; }

        /// <summary>
        ///     The total number of results.
        /// </summary>
        [JsonPropertyName("totalCount")]
        public int TotalCount { get; set; }

        /// <summary>
        ///     Indicates whether there are previous result pages.
        /// </summary>
        [JsonPropertyName("hasPrevious")]
        public bool HasPrevious => CurrentPage > 1;

        /// <summary>
        ///     Indicates whether there are further result pages.
        /// </summary>
        [JsonPropertyName("hasNext")]
        public bool HasNext => CurrentPage < TotalPages;

        #endregion
    }
}
