﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

CREATE TABLE "Paintballfields" (
    "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
    "Name" text NULL,
    "Street" text NULL,
    "HouseNumber" text NULL,
    "PostalCode" integer NOT NULL,
    "Place" text NULL,
    "PhoneNumber" text NULL,
    "UserIdentifier" text NULL,
    CONSTRAINT "PK_Paintballfields" PRIMARY KEY ("Id")
);

CREATE TABLE "Teams" (
    "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
    "Name" text NULL,
    "UserIdentifier" text NULL,
    CONSTRAINT "PK_Teams" PRIMARY KEY ("Id")
);

CREATE TABLE "Matches" (
    "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
    "MatchName" text NULL,
    "Date" timestamp without time zone NULL,
    "Guid" text NULL,
    "UserIdentifier" text NULL,
    "TeamAId" integer NOT NULL,
    "TeamBId" integer NOT NULL,
    "PaintballfieldId" integer NOT NULL,
    CONSTRAINT "PK_Matches" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Matches_Paintballfields_PaintballfieldId" FOREIGN KEY ("PaintballfieldId") REFERENCES "Paintballfields" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Matches_Teams_TeamAId" FOREIGN KEY ("TeamAId") REFERENCES "Teams" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Matches_Teams_TeamBId" FOREIGN KEY ("TeamBId") REFERENCES "Teams" ("Id") ON DELETE CASCADE
);

CREATE INDEX "IX_Matches_PaintballfieldId" ON "Matches" ("PaintballfieldId");

CREATE INDEX "IX_Matches_TeamAId" ON "Matches" ("TeamAId");

CREATE INDEX "IX_Matches_TeamBId" ON "Matches" ("TeamBId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20220109121501_initial', '3.1.11');

ALTER TABLE "Matches" DROP CONSTRAINT "FK_Matches_Paintballfields_PaintballfieldId";

DROP TABLE "Paintballfields";

DROP INDEX "IX_Matches_PaintballfieldId";

ALTER TABLE "Matches" DROP COLUMN "PaintballfieldId";

ALTER TABLE "Matches" ADD "Location" text NULL;

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20220927101300_remove-fields-entity', '3.1.11');

CREATE TABLE "Features" (
    "Id" integer NOT NULL GENERATED BY DEFAULT AS IDENTITY,
    "FeatureToggle" character varying(50) NOT NULL,
    "IsEnabled" boolean NOT NULL,
    CONSTRAINT "PK_Features" PRIMARY KEY ("Id")
);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20220928130201_Add-feature-toggle', '3.1.11');

