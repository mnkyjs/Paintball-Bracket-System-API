# Get the .NET SDK image for building
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env
WORKDIR /source

# Copy and publish app 
COPY . ./
RUN dotnet publish -c release -o /app

# Generate runtime image
FROM mcr.microsoft.com/dotnet/sdk:3.1
WORKDIR /app
EXPOSE 80
COPY --from=build-env /app .
ENTRYPOINT ["dotnet", "BracketSystem.Web.dll"]