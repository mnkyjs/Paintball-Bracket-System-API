﻿using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BracketSystem.Core.Data;
using BracketSystem.Core.Models;
using BracketSystem.Core.Models.Dtos;
using BracketSystem.Core.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BracketSystem.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TeamsController : ControllerBase
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion Fields

        #region Constructors

        public TeamsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion Constructors

        #region Methods

        [HttpPost(Name = "PostTeam")]
        public async Task<ActionResult<TeamDto>> Create(TeamDto teamDto)
        {
            teamDto.Name = teamDto.Name.ToLower(CultureInfo.InvariantCulture);
            var team = await _unitOfWork.Teams.FindByConditionSingle(x => x.Name == teamDto.Name).ConfigureAwait(false);
            if (team != null) return BadRequest($"{teamDto.Name} existiert bereits!");

            var teamToCreate = new Team();
            BaseDto.CopyProperties(teamDto, teamToCreate);
            teamToCreate.UserIdentifier = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            _unitOfWork.Teams.Add(teamToCreate);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return Ok(new TeamDto(teamToCreate));
        }

        [HttpDelete("{id}", Name = "DeleteTeam")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var team = await _unitOfWork.Teams.FindByConditionSingle(dbTeam => dbTeam.Id == id).ConfigureAwait(false);

            if (team == null)
                return NotFound("Kein Team gefunden");

            var isAdmin = User.Claims.FirstOrDefault(c => c.Type == "@roles")?.Value == "admin";
            if (team.UserIdentifier != User.FindFirst(ClaimTypes.NameIdentifier).Value && !isAdmin)
                return BadRequest();

            await _unitOfWork.Teams.DeleteObjectById(id).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return Ok(200);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<PagedResult<TeamDto>>> FindTeams(int page = 1, int pageSize = 10,
            string filter = null, string sortColumn = "Name", string sortOrder = "asc")
        {
            var teams = await _unitOfWork.Teams.FindTeams(page, pageSize, filter, sortColumn, sortOrder);
            var teamDtos = teams.ConvertAll(team => new TeamDto(team));
            var count = await _unitOfWork.Teams.CountTeamsAsync(filter);
            return Ok(new PagedResult<TeamDto>(teamDtos, count, page, pageSize));
        }

        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetTeamById")]
        public async Task<ActionResult<TeamDto>> GetSingleRecord(int id)
        {
            var teamDto =
                new TeamDto(await _unitOfWork.Teams.FindByConditionSingle(x => x.Id == id)
                    .ConfigureAwait(false));

            return Ok(teamDto);
        }

        [HttpPut("{id}", Name = "UpdateTeam")]
        public async Task<ActionResult<TeamDto>> PutAsync(int id, TeamDto teamDto)
        {
            teamDto.Name = teamDto.Name.ToLower(CultureInfo.InvariantCulture);
            var team = await _unitOfWork.Teams.GetById(id).ConfigureAwait(false);

            var isAdmin = User.Claims.FirstOrDefault(c => c.Type == "@roles")?.Value == "admin";
            if (team.UserIdentifier != User.FindFirst(ClaimTypes.NameIdentifier).Value && !isAdmin)
                return BadRequest();

            var teamNameCheck = await _unitOfWork.Teams.FindByConditionSingle(x => x.Name == teamDto.Name)
                .ConfigureAwait(false);
            if (teamNameCheck != null) return BadRequest($"{teamDto.Name} existiert bereits!");

            BaseDto.CopyProperties(teamDto, team);

            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return Ok(teamDto);
        }

        #endregion Methods
    }
}
