using System.Collections.Generic;
using System.Threading.Tasks;
using BracketSystem.Core.Data;
using BracketSystem.Core.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BracketSystem.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeatureController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public FeatureController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpGet(Name = "GetFeatures")]
        public async Task<IEnumerable<Feature>> GetFeatures()
        {
            var features = await _unitOfWork.Features.FindByConditionList().ConfigureAwait(true);
            return features;
        }
    }
}
