﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BracketSystem.Core.Data;
using BracketSystem.Core.Models.Dtos;
using BracketSystem.Core.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BracketSystem.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MatchesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public MatchesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost(Name = "CreateSchedule")]
        public async Task<ActionResult<IEnumerable<TeamDto[]>>> CreateSchedule(CreateScheduleDto createScheduleDto)
        {
            var teamDtos = createScheduleDto.Teams.ToList();
            var parsedDate = DateTime.Parse(createScheduleDto.Date, CultureInfo.InvariantCulture);
            var matches = await _unitOfWork.Matches.CreateSchedule(teamDtos, parsedDate,
                    User.FindFirst(ClaimTypes.NameIdentifier).Value,
                    createScheduleDto.Location, createScheduleDto.Name)
                .ConfigureAwait(false);

            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return Ok(matches);
        }

        [HttpDelete(Name = "DeleteAllMatch")]
        public async Task<IActionResult> DeleteAllMatch()
        {
            var matchesToDelete = await _unitOfWork.Matches
                .DeleteMatches(User.FindFirst(ClaimTypes.NameIdentifier).Value).ConfigureAwait(false);
            await _unitOfWork.Matches.RemoveRange(matchesToDelete).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return Ok(200);
        }

        [HttpDelete("{guid}", Name = "DeleteMatch")]
        public async Task<IActionResult> DeleteMatch(string guid)
        {
            var match = await _unitOfWork.Matches.FindByConditionSingle(match => match.Guid == guid).ConfigureAwait(false);

            var isAdmin = User.Claims.FirstOrDefault(c => c.Type == "@roles")?.Value == "admin";
            if (match.UserIdentifier != User.FindFirst(ClaimTypes.NameIdentifier).Value && !isAdmin)
                return BadRequest();
            var matches = await _unitOfWork.Matches
                .FindByConditionList(dbMatch => dbMatch.Guid == guid)
                .ConfigureAwait(false);

            await _unitOfWork.Matches.RemoveRange(matches).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return Ok(200);
        }

        [AllowAnonymous]
        [HttpGet("{guid}", Name = "GetMatchesByGuid")]
        public async Task<ActionResult<BlockForViewDto>> GetMatchesByGuid(string guid)
        {
            var matchesByGuid = await _unitOfWork.Matches.GetMatchesByGuid(guid).ConfigureAwait(true);
            return Ok(matchesByGuid);
        }

        [AllowAnonymous]
        [HttpGet(Name = "GetAllMatches")]
        public async Task<ActionResult<IEnumerable<MatchForViewDto>>> GetAllMatches()
        {
            var matches = await _unitOfWork.Matches.GetAllMatches().ConfigureAwait(true);
            return Ok(matches);
        }
    }
}
