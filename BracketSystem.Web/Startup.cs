﻿using System.Linq;
using System.Net;
using System.Text.Json.Serialization;
using BracketSystem.Core.Data;
using BracketSystem.Core.Data.Repositories;
using BracketSystem.Core.Helpers;
using BracketSystem.Core.Models.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BracketSystem.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    opt.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    opt.SerializerSettings.Formatting = Formatting.None;
                    opt.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                    opt.SerializerSettings.Converters.Add(new StringEnumConverter());
                });

            services.AddDbContext<BracketContext>(x =>
            {
                var connString = Configuration["DB_CONNECTION_STRING"] ??
                                 Configuration.GetConnectionString("DefaultConnection");
                x.UseNpgsql(connString);
                ConfigureServices(services);
            });

            var allowedOrigins = Configuration
                .GetSection("AllowedHosts")
                .GetChildren()
                .Select(x => x.Value)
                .ToArray();

            services.AddCors(options =>
            {
                options.AddPolicy("Cors",
                    corsPolicyBuilder => corsPolicyBuilder
                        .WithOrigins(allowedOrigins)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .Build());
            });

            AddAuthenticationScheme(services);

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            if (HostingEnvironment.IsDevelopment())
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo {Title = "BracketSystem API", Version = "v1"});
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseCors("Cors");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // enable swagger middleware
                app.UseSwagger();
                app.UseSwaggerUI(
                    setup =>
                    {
                        setup.SwaggerEndpoint("/swagger/v1/swagger.json", "BracketSystem API v1");
                        setup.RoutePrefix = string.Empty;
                    });
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context =>
                    {
                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();

                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(true);
                        }
                    });
                });
            }

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private void AddAuthenticationScheme(IServiceCollection services)
        {
            var domain = Configuration.GetValue<string>("Auth0:Domain");
            var audience = Configuration.GetValue<string>("Auth0:Audience");
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = audience;
            });
        }
    }
}
